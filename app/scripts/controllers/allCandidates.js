'use strict';

/**
 * @ngdoc function
 * @name techInterviewApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the techInterviewApp
 */
angular.module('techInterviewApp')
  .controller('AllCandidatesController', function ($scope) {
    $scope.candidateDetails = [
            {
                id: 1,
                firstName: 'Mike',
                lastName: 'Raffone',
                practice: 'AppDev',
                initialLevel: 'Staff I Consultant',
                techInterviewScore: 0,
                summary: 'Mike has 4 years of experience in web development. He is passionate and will to go above and beyond what it takes to learn.',
                skills: [
                    'HTML5',
                    'CSS3',
                    'JavaScript',
                    'jQuery',
                    'Rapid prototyping',
                    'UX',
                    'Responsive web design'
                ]
            }, 
            {
                id: 2,
                firstName: 'Justin',
                lastName: 'Case',
                practice: 'AppDev',
                initialLevel: 'Senior II Consultant',
                techInterviewScore: 0,
                summary: 'Justin has over 6 years of web developement experience. Solid employee. Reason for leaving, laid off.',
                skills: [
                    'HTML5',
                    'CSS3',
                    'Sass',
                    '.Net Development',
                    'WCF',
                    'C#',
                    'JavaScript',
                    'jQuery',
                ]
            },
            {
                id: 3,
                firstName: 'Brooke',
                lastName: 'Trout',
                practice: 'AppDev',
                initialLevel: 'Senior I Consultant',
                techInterviewScore: 0,
                summary: 'Tammy is a stellar employee at her current company. She has the drive and inititive.',
                skills: [
                    'HTML5',
                    'CSS3',
                    'JavaScript',
                    'jQuery',
                    'Rapid prototyping',
                    'UX',
                    'Responsive web design'
                ]
            }
        ];
    
        $scope.addCandidate = function() {
            $scope.candidateDetails.push({
                id: 4,
                firstName: 'Anita',
                lastName: 'Bath',
                practice: 'AppDev',
                initialLevel: 'Associate II',
                techInterviewScore: 0,
                summary: 'Solid understanding of HTML, CSS and JavaScript.',
                skills: [
                    'HTML5',
                    'CSS3',
                    'JavaScript'
                ]
            });
        };
        
        $scope.practiceOptions = [
            { id: 'AppDev', name: 'App Dev' },
            { id: 'BusinessProductivity', name: 'Business Productivity' },
            { id: 'Cloud', name: 'Cloud' },
            { id: 'Data Solutions', name: 'Data Solutions' },
            { id: 'Project Services', name:'Project Services' },
            { id: 'UX Design', name: 'UX Design' },
            { id: 'WEM', name: 'WEM' }
        ];
    
        $scope.levelOptions = [
            { id: 'AssociateI', name: 'Associate I' },
            { id: 'AssociateII', name: 'Associate II' },
            { id: 'StaffI', name: 'Staff I' },
            { id: 'StaffII', name: 'Staff II' },
            { id: 'PrincipleI', name:'Principle I' },
            { id: 'PrincipleII', name: 'Principle II' }
        ];
    
    });

/*
{ id: 1, 'label': 'AssociateI', 'name': 'Associate I' },
{ id: 2, 'label': 'AssociateII', 'name': 'Associate II' },
{ id: 3, 'label': 'StaffI', 'name': 'Associate I' },
{ id: 4, 'label': 'StaffII', 'name': 'Associate II' },
{ id: 5, 'label': 'PrincipleI', 'name': 'Associate I' },
{ id: 6, 'label': 'PrincipleII', 'name': 'Associate II' }
*/
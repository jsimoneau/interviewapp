'use strict';

/**
 * @ngdoc function
 * @name techInterviewApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the techInterviewApp
 */
angular.module('techInterviewApp')
  .controller('CandidateController', function ($scope) {
    $scope.candidateDetails = {
        id: 1,
        firstName: 'John',
        lastName: 'Simoneau',
        practice: 'AppDev',
        initialLevel: 'Staff I Consultant',
        summary: 'John has 4 years of experience in web development. He is passionate and will to go above and beyond what it takes to learn.',
        skills: [
            'HTML5',
            'CSS3',
            'JavaScript',
            'jQuery',
            'Rapid prototyping',
            'UX',
            'Responsive web design'
        ]
    };
  });

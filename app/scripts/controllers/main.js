'use strict';

/**
 * @ngdoc function
 * @name techInterviewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the techInterviewApp
 */
angular.module('techInterviewApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc overview
 * @name techInterviewApp
 * @description
 * # techInterviewApp
 *
 * Main module of the application.
 */
angular
    .module('techInterviewApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/all-candidates.html',
                controller: 'AllCandidatesController'
            })
            .when('/edit', {
                templateUrl: 'views/edit-profile.html',
                controller: 'EditProfileController'
            })
            .when('/add', {
                templateUrl: 'views/add-candidate.html',
                controller: 'AddCandidateController'
            })
            .otherwise({
                redirectTo: '/'
            });
    });